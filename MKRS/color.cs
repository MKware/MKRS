﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKRS
{
    public partial class color : Form
    {
        public color()
        {
            InitializeComponent();
        }
        private readonly Random rand = new Random();
        public int Random(int min, int max)
        {
            return rand.Next(min, max);
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            int rint = Random(0, 256); // 255 + 1 because C# randomizer works like that, it will select from 0 to 255
            int gint = Random(0, 256);
            int bint = Random(0, 256);
            var r = Convert.ToByte(rint);
            var g = Convert.ToByte(gint);
            var b = Convert.ToByte(bint);
            var rs = Convert.ToString(rint);
            var gs = Convert.ToString(gint);
            var bs = Convert.ToString(bint);
            var randcolor = Color.FromArgb(r, g, b);
            string hexstr = randcolor.R.ToString("X2") + randcolor.G.ToString("X2") + randcolor.B.ToString("X2");

            this.BackColor = randcolor;
            rgbval.Text = rs + ", " + gs + ", " + bs;
            hexval.Text = "#" + hexstr;
        }

        private void rgbval_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(rgbval.Text);
        }

        private void hexval_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(hexval.Text);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new colorAbout().Show();
        }
    }
}
