﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKRS
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void numbersBtn_Click(object sender, EventArgs e)
        {
            new numbers().Show();
        }

        private void nineball_Click(object sender, EventArgs e)
        {
            new nineball().Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new MKRSAbout().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new uuid().Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new color().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new dice().Show();
        }
    }
}
