﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

// Ребята не стоит вскрывать этот код. 
// Вы молодые, хакеры, вам все легко. Это не то. Это не Stuxnet и даже не шпионские программы ЦРУ. Сюда лучше не лезть. 
// Серьезно, любой из вас будет жалеть. Лучше закройте компилятор и забудьте что там писалось. 
// Я вполне понимаю что данным сообщением вызову дополнительный интерес, но хочу сразу предостеречь пытливых - стоп. Остальные просто не найдут. 

namespace MKRS
{
    public partial class numbers : Form
    {
        private bool graphicsDrawn = false;
        public numbers()
        {
            InitializeComponent();
        }

        private readonly Random rand = new Random();
        public int Random(int min, int max)
        {
            return rand.Next(min, max);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            fromField.Enabled = true;
            toField.Enabled = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            fromField.Enabled = false;
            toField.Enabled = false;
            fromField.Text = null;
            toField.Text = null;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            randField.Text = null;
            label4.Text = null;
            if (checkBox1.Checked == true)
            {
                clearButtonGrph(Color.FromArgb(64, 64, 64));
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("By enabling this option, random number will be protected from video/photo editing and memory editing. This option will draw 4 lines over random number and render it using C# DrawString.", "Protection", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private Font fnt = new Font("Consolas", 12);
        private Font fntNum = new Font("Consolas", 20);
        private void button1_Click(object sender, EventArgs e)
        {
            bool prt = checkBox1.Checked;
            
            switch (prt) 
            {
                case (true):
                    pictureBox1.Visible = true;
                    if (radioButton2.Checked == true)
                    {
                        string fromstr = fromField.Text;
                        string tostr = toField.Text;
                        if (String.IsNullOrEmpty(fromField.Text))
                        {
                            MessageBox.Show("\"From\" field can't be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        if (String.IsNullOrEmpty(toField.Text))
                        {
                            MessageBox.Show("\"To\" field can't be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        int fromint = Convert.ToInt32(fromstr);
                        int toint = Convert.ToInt32(tostr) + 1;    // + 1 because C# randomizer works like that. The randomizer still will work with given range
                        int randans = Random(fromint, toint);
                        string randansstr = Convert.ToString(randans);
                        pictureBox1.BackColor = Color.White;
                        pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
                        clearGraphics();
                    }
                    else
                    {
                        int randansint = Random(Int32.MinValue, Int32.MaxValue);
                        string randansstrm = Convert.ToString(randansint);
                        pictureBox1.BackColor = Color.White;
                        pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
                        label4.Text = randansstrm;
                        clearGraphics();
                    }
                    break;
                case (false):
                    pictureBox1.Visible = false;
                    if (radioButton2.Checked == true)
                    {
                        string fromstr = fromField.Text;
                        string tostr = toField.Text;
                        if (String.IsNullOrEmpty(fromField.Text))
                        {
                            MessageBox.Show("\"From\" field can't be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        if (String.IsNullOrEmpty(toField.Text))
                        {
                            MessageBox.Show("\"To\" field can't be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        int fromint = Convert.ToInt32(fromstr);
                        int toint = Convert.ToInt32(tostr) + 1; // + 1 because C# randomizer works like that. The randomizer still will work with given range
                        int randans = Random(fromint, toint);
                        string randansstr = Convert.ToString(randans);
                        randField.Text = randansstr;
                    }
                    else
                    {
                        int randansint = Random(Int32.MinValue, Int32.MaxValue);
                        string randansstrm = Convert.ToString(randansint);
                        randField.Text = randansstrm;
                    }
                    break;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            randField.Text = null;
            label4.Text = null;
        }
        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            int randansint = Random(Int32.MinValue, Int32.MaxValue);     
            Graphics g = e.Graphics;
            Color bg = Color.FromArgb(64, 64, 64);
            SolidBrush bgbrush = new SolidBrush(bg);
            int pos1x1 = Random(10, 81);
            int pos1x2 = Random(100, 301);
            int pos1x3 = Random(100, 301);
            int pos1x4 = Random(-200, -30);
            int pos2x1 = Random(1, 61);
            int pos2x2 = Random(150, 351);
            int pos2x3 = Random(150, 340);
            int pos2x4 = Random(-250, -45);
            int pos3x1 = Random(10, 90);
            int pos3x2 = Random(120, 320);
            int pos3x3 = Random(130, 340);
            int pos3x4 = Random(-210, -30);
            int pos4x1 = Random(5, 75);
            int pos4x2 = Random(140, 305);
            int pos4x3 = Random(135, 312);
            int pos4x4 = Random(-190, -34);
            g.FillRectangle(bgbrush, -1, -1, 538, 78);
            g.DrawString("mkware.eu.org",
                fnt, System.Drawing.Brushes.DarkGray, new Point(400, 50));
            if (radioButton1.Checked == true)
            {
                g.DrawString(Convert.ToString(randansint),
                fntNum, System.Drawing.Brushes.White, new Point(100, 30));
            }
            else
            {
                string fromstr = fromField.Text;
                string tostr = toField.Text;
                int fromint = Convert.ToInt32(fromstr);
                int toint = Convert.ToInt32(tostr) + 1;
                int randanscustom = Random(fromint, toint);
                string randansstrcustom = Convert.ToString(randanscustom);
                g.DrawString(randansstrcustom,
                fntNum, System.Drawing.Brushes.White, new Point(100, 30));
            }
            g.DrawLine(System.Drawing.Pens.Red, pos1x1, pos1x2,                            
                pos1x3, pos1x4);
            g.DrawLine(System.Drawing.Pens.LightGreen, pos2x1, pos2x2,
                pos2x3, pos2x4);
            g.DrawLine(System.Drawing.Pens.Orange, pos3x1, pos3x2,
                pos3x3, pos3x4);
            g.DrawLine(System.Drawing.Pens.Magenta, pos4x1, pos4x2,
                pos4x3, pos4x4);
            graphicsDrawn = true;
        }
        
        private void clearGraphics()                                                        //clearGraphics and clearButtonGrph are solution that were suggested by ChatGPT. Thanks!
        {
            if (graphicsDrawn)
            {
                graphicsDrawn = false;
                pictureBox1.Invalidate();
                using (var brush = new SolidBrush(Color.White))
                {
                    using (var graphics = pictureBox1.CreateGraphics())
                    {
                        graphics.FillRectangle(brush, pictureBox1.ClientRectangle);
                    }
                }
            }
        }
        private void clearButtonGrph(Color color)
        {
            using (var brush = new SolidBrush(color))
            {
                using (var graphics = pictureBox1.CreateGraphics())
                {
                    graphics.FillRectangle(brush, pictureBox1.ClientRectangle);
                }
            }
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new numbersAbout().Show();
        }
    }
}
