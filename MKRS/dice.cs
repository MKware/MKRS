﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MKRS
{
    public partial class dice : Form
    {
        public dice()
        {
            InitializeComponent();
        }
        private readonly Random rand = new Random();
        public int Random(int min, int max)
        {
            return rand.Next(min, max);
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            int diceval1 = Random(1, 7);
            int diceval2 = Random(1, 7);
            switch (diceval1)
            {
                case (1):
                    dicepos1.Image = Properties.Resources.dice1;
                    break;
                case (2):
                    dicepos1.Image = Properties.Resources.dice2;
                    break;
                case (3):
                    dicepos1.Image = Properties.Resources.dice3;
                    break;
                case (4):
                    dicepos1.Image = Properties.Resources.dice4;
                    break;
                case (5):
                    dicepos1.Image = Properties.Resources.dice5;
                    break;
                case (6):
                    dicepos1.Image = Properties.Resources.dice6;
                    break;
            }
            switch (diceval2)
            {
                case (1):
                    dicepos2.Image = Properties.Resources.dice1;
                    break;
                case (2):
                    dicepos2.Image = Properties.Resources.dice2;
                    break;
                case (3):
                    dicepos2.Image = Properties.Resources.dice3;
                    break;
                case (4):
                    dicepos2.Image = Properties.Resources.dice4;
                    break;
                case (5):
                    dicepos2.Image = Properties.Resources.dice5;
                    break;
                case (6):
                    dicepos2.Image = Properties.Resources.dice6;
                    break;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new Abouts.diceAbout().Show();
        }
    }
}
