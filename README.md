# MK Random Suite
[![Project Status: Moved to https://codeberg.org/MKware/MKRS – The project has been moved to a new location, and the version at that location should be considered authoritative.](https://www.repostatus.org/badges/latest/moved.svg)](https://www.repostatus.org/#moved) to [https://git.sr.ht/~mkware/mkrs](https://git.sr.ht/~mkware/mkrs)

***
## Description

All sorts of randomizers in one program.

![screenshot](https://i.imgur.com/cU8RGWC.png)

***

## Usage
Get `MKRS.exe` from "Releases"

Custom installer coming soon.

### Source code
1. Clone repository
2. Open `MKRS.sln` in VS 2013 or higher

### Compiling
1. Go to VS menu on `Build -> Build solution`
2. Compiled binary can be found in `bin` folder (Make sure that `Release` configuration is set)